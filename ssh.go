package goutils

import (
	"errors"
	"fmt"
	"time"

	"golang.org/x/crypto/ssh"
)

type SSHInfo struct {
	User     string
	Password string
	Host     string
	Port     string
}

func ExecSSHCommand(info *SSHInfo, cmd string) (bool, error) {
	//创建sshp登陆配置
	config := &ssh.ClientConfig{
		Timeout:         time.Second, //ssh 连接time out 时间一秒钟, 如果ssh验证错误 会在一秒内返回
		User:            info.User,
		HostKeyCallback: ssh.InsecureIgnoreHostKey(), //这个可以， 但是不够安全
		Auth:            []ssh.AuthMethod{ssh.Password(info.Password)},
	}

	//dial 获取ssh client
	addr := fmt.Sprintf("%s:%s", info.Host, info.Port)
	sshClient, err := ssh.Dial("tcp", addr, config)
	if err != nil {
		return false, errors.New("failed to create client," + err.Error())
	}
	defer sshClient.Close()

	//创建ssh-session
	session, err := sshClient.NewSession()
	if err != nil {
		return false, errors.New("failed to create session," + err.Error())
	}
	defer session.Close()

	//执行远程命令
	out, err := session.CombinedOutput(cmd)
	if err != nil {
		return false, errors.New(fmt.Sprintf("failed to execute remote command,%s,%s", err.Error(), string(out)))
	}

	return true, nil
}
