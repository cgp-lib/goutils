package goutils

import (
	"math"
	"strconv"
	"strings"
)

// Substr 截取字符串 start 起点下标 end 终点下标(不包括)
func Substr(str string, start int, end int) string {
	rs := []rune(str)
	length := len(rs)

	if start < 0 || start > length || end > length || math.Abs(float64(end)) > float64(length)  {
		return ""
	}

	if end < 0 {
		end = length + end
	}

	return string(rs[start:end])
}

func JionIntSlice(params []int) string {
	var strParams []string
	for _, val := range params {
		strParams = append(strParams, strconv.Itoa(val))
	}

	return strings.Join(strParams, ",")
}
