package goutils

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
	"syscall"
)

var (
	ErrProcessRunning = errors.New("process is running")
	ErrFileStale      = errors.New("pidfile exists but process is not running")
	ErrFileInvalid    = errors.New("pidfile has invalid contents")
)

// PidFileRemove Remove a pidfile
func PidFileRemove(filename string) error {
	return os.RemoveAll(filename)
}

// PidFileWrite Write a pidfile
func PidFileWrite(filename string, pid int) error {
	return ioutil.WriteFile(filename, []byte(fmt.Sprintf("%d\n", pid)), 0644)
}

// PidFileControl Control a pidfile
func PidFileControl(filename string, pid int, overwrite bool) error {
	// Check for existing pid
	oldpid, err := PidFileContents(filename)
	if err != nil {
		if os.IsNotExist(err) {
			return nil
		}

		return err
	}

	// We have a pid
	if oldpid != pid && PidIsRunning(oldpid) {
		return ErrProcessRunning
	}

	if !overwrite {
		return ErrFileStale
	}

	return nil
}

func PidFileContents(filename string) (int, error) {
	contents, err := ioutil.ReadFile(filename)
	if err != nil {
		return 0, err
	}

	pid, err := strconv.Atoi(strings.TrimSpace(string(contents)))
	if err != nil {
		return 0, ErrFileInvalid
	}

	return pid, nil
}

func PidIsRunning(pid int) bool {
	process, err := os.FindProcess(pid)
	if err != nil {
		return false
	}

	err = process.Signal(syscall.Signal(0))
	if err != nil && err.Error() == "no such process" {
		return false
	}

	if err != nil && err.Error() == "os: process already finished" {
		return false
	}

	return true
}
