package goutils

import (
	"encoding/hex"
	"net"
	"strings"
)

func MacFormat(mac string) (string, error) {
		mac = strings.ReplaceAll(mac, "-", "")
		mac = strings.ReplaceAll(mac, ":", "")
		buf, err := hex.DecodeString(mac)
		if err != nil {
			return "", err
		}

		return net.HardwareAddr(buf).String(), nil
}