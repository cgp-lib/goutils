package goutils

import (
	"fmt"
	"os/exec"
)

// ExecBashCmd 执行具体命令,返回结果
func ExecBashCmd(command string) (bool, string) {
	cmd := exec.Command("bash", "-c", command)
	out, err := cmd.CombinedOutput()
	if err != nil {
		return false, fmt.Sprintf("out:%s,err:%s", string(out), err.Error())
	}
	return true, string(out)
}
