package goutils

import (
	"fmt"
	"reflect"
)

func IfsSliceToStrSlice(ifs []interface{}) []string {
	s := make([]string, len(ifs))
	for i, v := range ifs {
		s[i] = fmt.Sprint(v)
	}
	return s
}

func IfsSliceToIntSlice(ifs []interface{}) []int {
	s := make([]int, len(ifs))
	for i, v := range ifs {
		s[i] = int(v.(float64))
	}
	return s
}

func StrSliceToMap(columns []string, needValue ...bool) map[string]interface{} {
	var isNeedValue bool
	if len(needValue) > 0 && needValue[0] {
		isNeedValue = true
	}

	m := make(map[string]interface{})
	for _, column := range columns {
		if _, ok := m[column]; !ok {
			if isNeedValue {
				m[column] = column
			} else {
				m[column] = ""
			}
		}
	}

	return m
}

func SliceContains(arr interface{}, val interface{}) bool {
	s := reflect.ValueOf(arr)
	for i := 0; i < s.Len(); i++ {
		if reflect.DeepEqual(val, s.Index(i).Interface()) {
			return true
		}
	}

	return false
}

func InSlice(v interface{}, sl []interface{}) bool {
	for _, vv := range sl {
		if vv == v {
			return true
		}
	}
	return false
}