package goutils

import "reflect"

func FillMap(sourceMap map[string]interface{}, fillColumns []string, targetMap ...map[string]interface{}) map[string]interface{} {
	var target = make(map[string]interface{})
	if len(targetMap) > 0 {
		target = targetMap[0]
	}

	for _, column := range fillColumns {
		if _, ok := sourceMap[column]; ok {
			target[column] = sourceMap[column]
		}
	}

	return target
}

func MapHasKey(m map[string]interface{}, key string) bool {
	for k, _ := range m {
		if k == key {
			return true
		}
	}

	return false
}

func MapContainsValue(m interface{}, val interface{}) bool {
	s := reflect.ValueOf(m).MapRange()
	for s.Next() {
		if reflect.DeepEqual(val, s.Value().Interface()) {
			return true
		}
	}

	return false
}

// GetMapValues 得到数组的值的数组
func GetMapValues(data map[string]string) (result []string) {
	for _, value := range data {
		result = append(result, value)
	}
	return
}

func PluckMap(rawMap map[string]interface{}, keys []string) map[string]interface{} {
	var m = make(map[string]interface{})

	for _, k := range keys {
		if _, ok := rawMap[k]; ok {
			m[k] = rawMap[k]
		}
	}

	return m
}
