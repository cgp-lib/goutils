package goutils

import "time"

func RFC3339ToCSTFormat(rfc3339 string) (string, error) {
	to, err := time.ParseInLocation(time.RFC3339, rfc3339, time.Local)
	if err != nil {
		return "", err
	}
	return to.Format(CST), nil
}

// TimeFormat 转换时间格式
func TimeFormat(oldLayout, newLayout, timeStr string) (string, error) {
	times, err := time.ParseInLocation(oldLayout, timeStr, time.Local)
	if err != nil {
		return "", err
	}
	return times.Format(newLayout), nil
}
