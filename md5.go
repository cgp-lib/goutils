package goutils

import (
	"crypto/md5"
	"encoding/hex"
	"io/ioutil"
	"strings"
)

func MD5Encode(value string) string {
	m := md5.New()
	m.Write([]byte(value))

	return hex.EncodeToString(m.Sum(nil))
}

func CheckMD5(content, encrypted string) bool {
	return strings.EqualFold(MD5Encode(content), encrypted)
}

func Md5File(file string) (string, error) {
	data, err := ioutil.ReadFile(file)
	if err != nil {
		return "", err
	}

	value := md5.Sum(data)

	return hex.EncodeToString(value[:]), nil
}
