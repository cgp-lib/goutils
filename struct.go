package goutils

import (
	"errors"
	"fmt"
	"reflect"
	"strconv"
	"strings"
)

// PluckStructWithJsonByMap 使用map填充结构体, obj为指针
func PluckStructWithJsonByMap(obj interface{}, rawMap map[string]interface{}) {
	v := reflect.ValueOf(obj).Elem()
	t := v.Type()
	fieldNum := v.NumField()
	for i := 0; i < fieldNum; i++ {
		fieldInfo := t.Field(i)
		tag := fieldInfo.Tag.Get("json")
		if tag == "" {
			continue
		}
		if value, ok := rawMap[tag]; ok {
			if reflect.ValueOf(value).Type() == v.FieldByName(fieldInfo.Name).Type() {
				v.FieldByName(fieldInfo.Name).Set(reflect.ValueOf(value))
			}
		}
	}
}

func Struct2Map(in interface{}) map[string]interface{} {
	out := make(map[string]interface{})

	v := reflect.ValueOf(in)

	if v.Kind() == reflect.Ptr {
		v = v.Elem()
	}

	if v.Kind() != reflect.Struct {
		return out
	}

	t := v.Type()

	for i := 0; i < v.NumField(); i++ {
		fieldValue := v.Field(i)

		if fieldValue.Kind() == reflect.Struct {
			for key, val := range Struct2Map(fieldValue.Interface()) {
				out[key] = val
			}
		} else {
			fieldInfo := t.Field(i)

			name := fieldInfo.Name
			tag := fieldInfo.Tag.Get("json")
			if tag != "" {
				name = strings.Split(tag, ",")[0]
			}
			out[name] = v.Field(i).Interface()
		}
	}

	return out
}

// FillStructFromMap 将 map 数据填充到结构体中
func FillStructFromMap(obj interface{}, dstMap map[string]interface{}) (returnErr error) {
	defer func() {
		if r := recover(); r != nil {
			returnErr = fmt.Errorf("failed to set value: %v", r)
		}
	}()

	objValue := reflect.ValueOf(obj)
	if objValue.Kind() != reflect.Ptr || objValue.IsNil() {
		return errors.New("invalid object: must be a non-nil pointer")
	}

	objValue = objValue.Elem()
	objType := objValue.Type()

	for i := 0; i < objValue.NumField(); i++ {
		typeField := objType.Field(i)
		jsonKey := typeField.Tag.Get("json")

		// 处理 json tag
		if jsonKey != "" {
			jsonKey = strings.Split(jsonKey, ",")[0]
		}
		if jsonKey == "" {
			jsonKey = typeField.Name
		}

		mapValue, ok := dstMap[jsonKey]
		if !ok {
			continue
		}

		valueField := objValue.Field(i)

		switch valueField.Kind() {
		case reflect.Ptr:
			// 指针类型使用valueField.Interface()获取属性的指针
			newInstance := valueField.Interface()

			// 处理指针类型的结构体，实例化一个结构体指针并赋值给当前属性
			if valueField.IsNil() {
				newInstance = reflect.New(valueField.Type().Elem()).Interface()
				valueField.Set(reflect.ValueOf(newInstance))
			}

			if valueField.Type().Elem().Kind() == reflect.Struct {
				// 如果mapValue不是nil，递归填充子结构
				if subMap, ok := mapValue.(map[string]interface{}); ok {
					if err := FillStructFromMap(newInstance, subMap); err != nil {
						return err
					}
				}
			} else {
				if err := setValue(valueField.Elem(), mapValue); err != nil {
					return err
				}
			}
		case reflect.Struct:
			// 结构体类型使用valueField.Addr().Interface()获取当前属性的指针 传递给递归
			if subMap, ok := mapValue.(map[string]interface{}); ok {
				if err := FillStructFromMap(valueField.Addr().Interface(), subMap); err != nil {
					return err
				}
			}
		default:
			if err := setValue(valueField, mapValue); err != nil {
				return err
			}
		}
	}
	return nil
}

// setValue 设置字段值
func setValue(field reflect.Value, value interface{}) (returnErr error) {
	switch field.Kind() {
	case reflect.String:
		field.SetString(fmt.Sprintf("%v", value))
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		intValue, err := strconv.ParseInt(fmt.Sprintf("%v", value), 10, 64)
		if err != nil {
			return fmt.Errorf("failed to parse int: %v", err)
		}
		field.SetInt(intValue)
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		uintValue, err := strconv.ParseUint(fmt.Sprintf("%v", value), 10, 64)
		if err != nil {
			return fmt.Errorf("failed to parse uint: %v", err)
		}
		field.SetUint(uintValue)
	case reflect.Float32, reflect.Float64:
		floatValue, err := strconv.ParseFloat(fmt.Sprintf("%v", value), 64)
		if err != nil {
			return fmt.Errorf("failed to parse float: %v", err)
		}
		field.SetFloat(floatValue)
	case reflect.Slice:
		slice, ok := value.([]interface{})
		if !ok {
			return errors.New("invalid slice value")
		}
		sliceValue := reflect.MakeSlice(field.Type(), len(slice), len(slice))
		for i, v := range slice {
			if err := setValue(sliceValue.Index(i), v); err != nil {
				return err
			}
		}
		field.Set(sliceValue)
	case reflect.Array:
		arrLen := field.Len()
		arrSlice, ok := value.([]interface{})
		if !ok || len(arrSlice) != arrLen {
			return errors.New("array length mismatch")
		}
		for i := 0; i < arrLen; i++ {
			if err := setValue(field.Index(i), arrSlice[i]); err != nil {
				return err
			}
		}
	case reflect.Map:
		if field.Type().String() == reflect.TypeOf(value).String() {
			field.Set(reflect.ValueOf(value))
			return nil
		}
		mapInterface, ok := value.(map[string]interface{})
		if !ok {
			return errors.New("invalid map value")
		}
		newMap, err := createNewMap(field.Type(), mapInterface)
		if err != nil {
			return err
		}
		field.Set(newMap)
	default:
		field.Set(reflect.ValueOf(value))
	}
	return nil
}

// createNewMap 创建新的映射
func createNewMap(mapType reflect.Type, mapInterface map[string]interface{}) (reflect.Value, error) {
	newMap := reflect.MakeMap(mapType)
	keyType := mapType.Key()
	elemType := mapType.Elem()

	for keyStr, elemValue := range mapInterface {
		key, err := convertToKeyType(keyStr, keyType)
		if err != nil {
			return reflect.Value{}, err
		}
		val, err := convertToValueType(elemValue, elemType)
		if err != nil {
			return reflect.Value{}, err
		}
		newMap.SetMapIndex(key, val)
	}
	return newMap, nil
}

// convertToKeyType 转换键类型
func convertToKeyType(keyStr string, keyType reflect.Type) (reflect.Value, error) {
	switch keyType.Kind() {
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		keyInt, err := strconv.ParseInt(keyStr, 10, keyType.Bits())
		if err != nil {
			return reflect.Value{}, fmt.Errorf("cannot parse %s as int: %v", keyStr, err)
		}
		return reflect.ValueOf(keyInt).Convert(keyType), nil
	case reflect.String:
		return reflect.ValueOf(keyStr).Convert(keyType), nil
	default:
		return reflect.Value{}, fmt.Errorf("unsupported key type: %s", keyType.Kind())
	}
}

// convertToValueType 转换值类型
func convertToValueType(value interface{}, valueType reflect.Type) (reflect.Value, error) {
	switch valueType.Kind() {
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		intValue, ok := value.(float64)
		if !ok {
			return reflect.Value{}, fmt.Errorf("cannot convert %v to int", value)
		}
		return reflect.ValueOf(int64(intValue)).Convert(valueType), nil
	case reflect.String:
		strValue, ok := value.(string)
		if !ok {
			return reflect.Value{}, fmt.Errorf("cannot convert %v to string", value)
		}
		return reflect.ValueOf(strValue).Convert(valueType), nil
	default:
		return reflect.Value{}, fmt.Errorf("unsupported value type: %s", valueType.Kind())
	}
}
