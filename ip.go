package goutils

import (
	"bytes"
	"errors"
	"fmt"
	"math/big"
	"net"
	"net/netip"
	"strconv"
	"strings"
)

// IsIPV4 校验ip是否为ipv4格式
func IsIPV4(ip string) bool {
	trial := net.ParseIP(ip)
	if trial.To4() == nil {
		return false
	}
	return true
}

func GetLocalIP() (string, error) {
	adds, err := net.InterfaceAddrs()
	if err != nil {
		return "", err
	}

	for _, a := range adds {
		if inet, ok := a.(*net.IPNet); ok && !inet.IP.IsLoopback() {
			if inet.IP.To4() != nil {
				return inet.IP.String(), nil
			}
		}
	}

	return "", errors.New("not found")
}

// InIPRange 判断ip是否在区间里
// ipRange 以[-]分割的起始结束ip
func InIPRange(ip string, ipRange string) (inRange bool) {
	trial := net.ParseIP(ip)
	if trial.To4() == nil {
		return false
	}

	rangeArr := strings.Split(ipRange, "-")
	start := net.ParseIP(rangeArr[0])
	if start.To4() == nil {
		return false
	}
	end := net.ParseIP(rangeArr[1])
	if end.To4() == nil {
		return false
	}

	if bytes.Compare(trial, start) >= 0 && bytes.Compare(trial, end) <= 0 {
		return true
	}

	return false
}

func ParseIPToUint32(ip string) uint32 {
	var uintIP uint32
	ipObj := net.ParseIP(ip)
	if ipObj == nil {
		return 0
	}

	uintIP |= uint32(ipObj[12]) << 24
	uintIP |= uint32(ipObj[13]) << 16
	uintIP |= uint32(ipObj[14]) << 8
	uintIP |= uint32(ipObj[15])

	return uintIP
}

func IPToNetworkAddress(ip, netmask string) string {
	networkAddressInt := big.NewInt(0).SetBytes(net.ParseIP(ip)).Int64() & big.NewInt(0).SetBytes(net.ParseIP(netmask)).Int64()
	return fmt.Sprintf("%d.%d.%d.%d", byte(networkAddressInt>>24), byte(networkAddressInt>>16), byte(networkAddressInt>>8), byte(networkAddressInt))
}

func NetmaskToBits(netmask string) int {
	netmaskArr := strings.Split(netmask, ".")
	m0, _ := strconv.Atoi(netmaskArr[0])
	m1, _ := strconv.Atoi(netmaskArr[1])
	m2, _ := strconv.Atoi(netmaskArr[2])
	m3, _ := strconv.Atoi(netmaskArr[3])

	ones, _ := net.IPNet{Mask: net.IPv4Mask(byte(m0), byte(m1), byte(m2), byte(m3))}.Mask.Size()

	return ones
}

func ContainsIP(CIDR, ip string) (bool, error) {
	p, err := netip.ParsePrefix(CIDR)
	if err != nil {
		return false, err
	}
	a, err := netip.ParseAddr(ip)
	if err != nil {
		return false, err
	}

	return p.Contains(a), nil
}

func ContainsIPRange(CIDR, ipStart string, ipEnd string) (bool, error) {
	p, err := netip.ParsePrefix(CIDR)
	if err != nil {
		return false, err
	}

	s, err := netip.ParseAddr(ipStart)
	if err != nil {
		return false, err
	}

	e, err := netip.ParseAddr(ipEnd)
	if err != nil {
		return false, err
	}

	return p.Contains(s) && p.Contains(e), nil
}

// IpNetRange 返回子网的起始IP、结束IP
func IpNetRange(ipNet *net.IPNet) (start, end string) {
	mask := ipNet.Mask
	broadcast := copyIP(ipNet.IP)
	for i := 0; i < len(mask); i++ {
		ipIdx := len(broadcast) - i - 1
		broadcast[ipIdx] = ipNet.IP[ipIdx] | ^mask[len(mask)-i-1]
	}
	return ipNet.IP.String(), broadcast.String()
}

func copyIP(ip net.IP) net.IP {
	dup := make(net.IP, len(ip))
	copy(dup, ip)
	return dup
}

// ContainsCIDR 验证子网是否为另一个的子网
func ContainsCIDR(a, b *net.IPNet) bool {
	ones1, _ := a.Mask.Size()
	ones2, _ := b.Mask.Size()
	return ones1 <= ones2 && a.Contains(b.IP)
}

// ContainsCIDRString 验证子网是否为另一个的子网
func ContainsCIDRString(a, b string) (bool, error) {
	_, net1, err := net.ParseCIDR(a)
	if err != nil {
		return false, err
	}

	_, net2, err := net.ParseCIDR(b)
	if err != nil {
		return false, err
	}

	result := ContainsCIDR(net1, net2)
	return result, err
}

// IsSameNetworkSegment 判断两个ip是否同一网段
func IsSameNetworkSegment(a, b, mask string) bool {
	return IPToNetworkAddress(a, mask) == IPToNetworkAddress(b, mask)
}

func ExistsIntersection(r1, r2 string) (bool, error) {
	r1Start, r1End := splitIPRange(r1)
	r2Start, r2End := splitIPRange(r2)

	if r1Start == 0 || r1End == 0 || r2Start == 0 || r2End == 0 {
		return false, errors.New("invalid ip range")
	}

	if (r1Start >= r2Start && r1Start <= r2End || r1End >= r2Start && r1End <= r2End) ||
		(r2Start >= r1Start && r2Start <= r1End || r2End >= r1Start && r2End <= r1End) {
		return true, nil
	}

	return false, nil
}

func splitIPRange(r string) (start, end uint32) {
	rSplit := strings.Split(r, "-")
	if len(rSplit) == 2 {
		return ParseIPToUint32(rSplit[0]), ParseIPToUint32(rSplit[1])
	}

	rSplit = strings.Split(r, "/")
	if len(rSplit) == 2 {
		_, ipNet, err := net.ParseCIDR(r)
		if err != nil {
			return 0, 0
		}
		startIP, endIP := IpNetRange(ipNet)
		return ParseIPToUint32(startIP), ParseIPToUint32(endIP)
	}

	return 0, 0
}
