package goutils

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strings"
)

// PathExists 判断文件/目录是否存在
func PathExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil || !os.IsNotExist(err) {
		return true, nil
	}
	return false, err
}

// CreateDir 创建文件夹
func CreateDir(dir string) (bool, error) {
	err := os.MkdirAll(dir, os.ModePerm)
	if err != nil {
		return false, err
	}

	return true, nil
}

// RemoveFile 删除文件
func RemoveFile(fileName string) (bool, error) {
	if isExist, err := PathExists(fileName); !isExist {
		return false, err
	}

	if err := os.Remove(fileName); err != nil {
		return false, err
	}

	return true, nil
}

// WriteAppendFile 追加方式写文件
func WriteAppendFile(filename string, data []byte, perm ...os.FileMode) (bool, error) {
	if len(perm) == 0 {
		perm = make([]os.FileMode, 1)
		perm[0] = 0755
	}

	f, err := os.OpenFile(filename, os.O_WRONLY|os.O_CREATE|os.O_APPEND, perm[0])
	if err != nil {
		return false, err
	}
	defer f.Close()

	n, err := f.Write(data)
	if err != nil {
		return false, err
	}

	if err == nil && n < len(data) {
		return false, io.ErrShortWrite
	}

	return true, nil
}

// WriteTruncFile 覆盖方式写文件
func WriteTruncFile(filename string, data []byte, perm ...os.FileMode) (bool, error) {
	if len(perm) == 0 {
		perm = make([]os.FileMode, 1)
		perm[0] = 0755
	}

	f, err := os.OpenFile(filename, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, perm[0])
	if err != nil {
		return false, err
	}
	defer f.Close()

	n, err := f.Write(data)
	if err != nil {
		return false, err
	}

	if err == nil && n < len(data) {
		return false, io.ErrShortWrite
	}

	return true, nil
}

// MoveFile 复制文件
func MoveFile(srcName string, dstName string) (bool, error) {
	cmd := fmt.Sprintf(" mv -f '%s' '%s' ",
		srcName,
		dstName)

	ok, out := ExecBashCmd(cmd)
	if !ok {
		return false, errors.New(fmt.Sprintf("command:%s,mv failed,%s", cmd, out))
	}

	return true, nil
}

// CopyFile 复制文件
func CopyFile(srcName string, dstName string) (bool, error) {
	cmd := fmt.Sprintf(" cp '%s' '%s' ",
		srcName,
		dstName)

	ok, out := ExecBashCmd(cmd)
	if !ok {
		return false, errors.New(fmt.Sprintf("command:%s,mv failed,%s", cmd, out))
	}

	return true, nil
}

func IsDir(path string) (bool, error) {
	fi, err := os.Stat(path)
	if err != nil {
		return false, err
	}

	return fi.IsDir(), nil
}

func ReadLastLine(fileName string) (string, error) {
	fi, err := os.Open(fileName)
	if err != nil {
		return "", err
	}
	defer fi.Close()

	br := bufio.NewReader(fi)
	var lastLine string
	for {
		line, _, c := br.ReadLine()
		if c == io.EOF {
			break
		} else {
			lastLine = strings.TrimSpace(string(line))
		}
	}

	return lastLine, nil
}

func ReadSpecificLine(fileName string, keyword string) (str string, err error) {
	fi, err := os.Open(fileName)
	if err != nil {
		return
	}
	defer fi.Close()

	br := bufio.NewReader(fi)
	for {
		line, _, c := br.ReadLine()
		if strings.Contains(strings.TrimSpace(string(line)), keyword) {
			str = strings.TrimSpace(string(line))
			break
		}
		if c == io.EOF {
			break
		}
	}

	return str, nil
}

func ReadFileToMap(fileName string) (map[string]string, error) {
	data := make(map[string]string)
	fi, err := os.Open(fileName)
	if err != nil {
		return nil, err
	}
	defer fi.Close()

	br := bufio.NewReader(fi)
	for {
		line, _, c := br.ReadLine()
		if c == io.EOF {
			break
		} else {
			line := strings.TrimSpace(string(line))
			data[line] = line
		}
	}

	return data, nil
}

func ReadFileToSlice(fileName string) ([]string, error) {
	var arr []string

	fi, err := os.Open(fileName)
	if err != nil {
		return nil, err
	}
	defer fi.Close()

	br := bufio.NewReader(fi)
	for {
		line, _, c := br.ReadLine()
		if c == io.EOF {
			break
		} else {
			line := strings.TrimSpace(string(line))
			arr = append(arr, line)
		}
	}

	return arr, nil
}

func GetFileContents(filename string) ([]string, error) {
	fi, err := os.Open(filename)
	if err != nil {
		return nil, err
	}

	lines, err := ioutil.ReadAll(fi)
	if err != nil {
		return nil, err
	}

	return strings.Split(string(lines), "\n"), nil
}

func ReadFileContents(filename string) ([]byte, error) {
	fi, err := os.Open(filename)
	if err != nil {
		return nil, err
	}

	bytes, err := ioutil.ReadAll(fi)
	if err != nil {
		return nil, err
	}

	return bytes, nil
}
