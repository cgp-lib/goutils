package goutils

import (
	"errors"
	"io/ioutil"
	"net"
	"os"
	"path"
	"time"

	"github.com/pkg/sftp"
	"golang.org/x/crypto/ssh"
)

// Sftp连接
// user 远程服务器用户名
// password 密码
// host 主机ip
// port 端口
func SftpConnect(info *SSHInfo) (sftpClient *sftp.Client, err error) {
	auth := make([]ssh.AuthMethod, 0)
	auth = append(auth, ssh.Password(info.Password))

	clientConfig := &ssh.ClientConfig{
		User:    info.User,
		Auth:    auth,
		Timeout: 30 * time.Second,
		HostKeyCallback: func(hostname string, remote net.Addr, key ssh.PublicKey) error {
			return nil
		},
	}

	addr := info.Host + ":" + info.Port

	// 连接ssh
	sshClient, err := ssh.Dial("tcp", addr, clientConfig)

	if err != nil {
		return nil, errors.New("ssh connection failed," + err.Error())
	}

	// 创建客户端
	if sftpClient, err = sftp.NewClient(sshClient); err != nil {
		return nil, errors.New("failed to create client," + err.Error())
	}

	return
}

// 文件下载
// sftpClient 客户端
// localPath 本地路径
// remotePath 远程文件
func Download(sftpClient *sftp.Client, localPath, remoteFile string, modePerm ...os.FileMode) (string, error) {
	defer sftpClient.Close()

	srcFile, err := sftpClient.Open(remoteFile)
	if err != nil {
		return "", errors.New("read file failed," + err.Error())
	}
	defer srcFile.Close()
	localFilename := path.Base(remoteFile)
	fileName := path.Join(localPath, localFilename)

	if ok, _ := PathExists(localPath); !ok {
		return "", errors.New("localPath not exists")
	}

	dstFile, err := os.Create(fileName)
	if err != nil {
		return "", errors.New("create file failed," + err.Error())
	}
	defer dstFile.Close()

	if _, err := srcFile.WriteTo(dstFile); err != nil {
		return "", errors.New("write file failed," + err.Error())
	}

	if len(modePerm) > 0 {
		err = dstFile.Chmod(modePerm[0])
		if err != nil {
			return "", errors.New("chmod file failed," + err.Error())
		}
	}

	return fileName, nil
}

// 文件上传
func UploadFile(sftpClient *sftp.Client, localFileName, remotePath string) (string, error) {
	defer sftpClient.Close()

	remoteFileName := path.Base(localFileName)

	fileName := path.Join(remotePath, remoteFileName)
	dstFile, err := sftpClient.Create(fileName)
	if err != nil {
		return "", errors.New("create file failed," + err.Error())
	}
	defer dstFile.Close()

	srcFile, err := ioutil.ReadFile(localFileName)
	if err != nil {
		return "", errors.New("read file failed," + err.Error())
	}

	_, err = dstFile.Write(srcFile)
	if err != nil {
		return "", errors.New("write file failed," + err.Error())
	}

	return fileName, nil
}
